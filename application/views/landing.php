<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.min.css"> 
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <title>Landing Page</title>

    
  </head>
  <body style="font-family: Montserrat;">

    
    <nav class="navbar navbar-inverse" style="border-radius: 0px; margin-bottom: 0px;">
      <div class="container">
  <div class="container-fluid">
    <div class="navbar-header">
      
    </div>
    
    <ul class="nav navbar-nav navbar-right">
      <li><a href="signup.html"><span class="glyphicon glyphicon-user"></span> Register</a></li>
      <li><a href="<?= base_url('Auth') ?>"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
    </ul>
  </div>
</div>
</nav>

    
    <div id="k1" style="background-image: url('assets/image/bg1.jpg');">
      <div class="header">

        <div class="col-md-6">
        
        <img src="assets/image/s1.png" class="s1">

        </div> 
        
        <div class="col-md-6">

        <div class="your">
        <p>Your Inventory</p>
        <p>In Here.</p>

        <p class="can"> <em>Invent .</em> makes it easier for us to see and borrow  </p> 
        <p class="can">goods in the school inventory .</p>
        </div>

        </div>

      </div>
    </div>

    <div id="k2_area">

<div class="whats" style="background-color: #FF6464; padding-top: 20px;"><center>Whats On <em>Invent .</em></center></div>

    <div class="col-md-6 k2">
       <div class="wecan">
            <center>
            <p>we can see a list of items that</p>
            <p>available at school</p>
            <p>with excellent conditions</p>
            <p>and good storage.</p>
            <p><em>Invent. </em> can make it easier for</p>
            <p>us to find the items </p>
            <p>we need in a short time.</p>
          </center>
          </div>
    </div>

    <div class="col-md-6 k2">
      <div class="wecan">
            <center>
            <p>we can borrow goods that</p>
            <p>are in the</p>
            <p>inventory list. Borrowing is</p>
            <p>done easily with</p>
            <p>an easy return. </p>
            <p>The items we borrow  </p>
            <p>are of very good quality.</p>
          </center>
          </div>
    </div>

</div>

<div class="parallax" style="background-image: url('assets/image/bg1.jpg');">
<br>
  <div class="whats" style="margin-bottom: 70px;"><center>How To Use <em>Invent .</em> ?</center></div>
  
          <div class="col-sm-4">
              <center>
            <div class="thumbnail">
              <img src="assets/image/lihat.png" alt="..." class="us">
              
            </div>
            <p class="how">1. See the list inventory </p>
              </center>
          </div>

          <div class="col-sm-4">
            <center>
            <div class="thumbnail">
              <img src="assets/image/pilih.png" alt="..." class="us">
              
              
            </div>
            <p class="how">2. Pick the things </p>
              </center>
          </div>

          <div class="col-sm-4">
            <center>
            <div class="thumbnail">
              <img src="assets/image/pinjam.png" alt="..." class="us">
              
              
            </div>
            <p class="how">3. Borrow the things</p>
              </center>
          </div>


          <div class="lets">

            <center>  <a href="signin.html"><p class="blets"> <b> Let's see what you need now!</b></p></center></a>

         </div>


</div>

<div class="whats" style="background-color: #FF6464; padding-top: 20px;"><center>Our Officer</center></div>
  
  <center>
      <div class="col-md-6 k2">
        <div class="thumbnail">
        </div>
        <p class="how">- Admin Name -</p>
      </div>

      <div class="col-md-6 k2">
        <div class="thumbnail">              
        </div>
        <p class="how">- Officer Name -</p>
      </div>
  </center>



  <div id="footer" style="background-image: url('assets/image/bg3.jpg');">
        <div class="f_area">
          
          <div class="col-md-4"> 
            <img src="assets/image/s1.png" class="fimg">
          </div>

          <div class="col-md-8"> 

            <div class="sm">
                <center>
                <a href="#" class="fa fa-facebook"></a>
                <a href="#" class="fa fa-twitter"></a>
                <a href="#" class="fa fa-linkedin"></a>
                <a href="#" class="fa fa-instagram"></a>
                </center>
            </div>

            <center><p class="copy"><b>Copyright Since 2019</b></p></center>

          </div>


        </div>
      </div>
  


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script type="text/javascript" src="assets/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="assets/bootstrap/js/npm.js"></script>
    <script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
    
  </body>
</html>