<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/bootstrap/css/bootstrap.min.css"> 
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/admin.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <title>Inventory List. Admin</title>

    
  </head>
  <body style="font-family: Montserrat;">

    
      <div class="container-fluid">
      <div class="row">
      <div class="col-sm-3 col-md-2 sidebar" style="background-color: black;">

<ul class="nav nav-sidebar">
  <li><a href="#" style="color:white; font-size: 25pt;margin-top: 60px; margin-left: 20px;">Invent.</a></li>
  <li><a href="#">username</a></li>
  <li><a href="#">Role : Admin</a></li>
  <li><a href="<?= base_url('Landing') ?>">Logout<i class="glyphicon glyphicon-log-out" style="margin-left: 10px;"></i></a></li>
</ul>

<ul class="nav nav-sidebar">
  <li><a href="<?= base_url('Dashadmin') ?>" style="color:white;"><i class="glyphicon glyphicon-home" style="margin-right: 10px;"></i>Home </a></li>
</ul>

<ul class="nav nav-sidebar">
  <li><p href="#" style="color:white; font-size: 18pt; margin-left: 20px; ">Inventory</p></li>
  <li><a href="<?= base_url('Inventadmin') ?>" style="color:white;"><i class="glyphicon glyphicon-list" style="margin-right: 10px;"></i>Inventory List </a></li>
  <li><a href="<?= base_url('Pjmadmin') ?>" style="color:white;"><i class="glyphicon glyphicon-list" style="margin-right: 10px;"></i>Borrow List</a></li>
  <li><a href="<?= base_url('Pkbadmin') ?>" style="color:white;"><i class="glyphicon glyphicon-list" style="margin-right: 10px;"></i>Return List</a></li>
</ul>

<ul class="nav nav-sidebar">
  <li><p href="#" style="color:white; font-size: 18pt; margin-left: 20px; ">User</p></li>
  <li><a href="#" style="color:white;"><i class="glyphicon glyphicon-list" style="margin-right: 10px;"></i>Admin Data </a></li>
  <li><a href="<?= base_url('Ptgadmin') ?>" style="color:white;"><i class="glyphicon glyphicon-list" style="margin-right: 10px;"></i>Officer Data</a></li>
  <li><a href="<?= base_url('Pminjamadmin') ?>" style="color:white;"><i class="glyphicon glyphicon-list" style="margin-right: 10px;"></i>Borrowers Data</a></li>
</ul>


</div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">


          <h1 class="page-header">Dashboard</h1>

          <div class="col-sm-4">
            <center>
              <div class="thumbnail jumlah">
                <h6>number of borrowers today :</h6>
                <h4 style="font-size: 30pt">10</h4>
              </div>
            </center>
          </div>

          <div class="col-sm-4">
            <center>
              <div class="thumbnail jumlah">
                <h6>number of borrows today :</h6>
                <h4 style="font-size: 30pt">10</h4>
              </div>
            </center>
          </div>

          <div class="col-sm-4" style="margin-bottom: 50px;">
            <center>
              <div class="thumbnail jumlah">
                <h6>number of returns today :</h6>
                <h4 style="font-size: 30pt">10</h4>
              </div>
            </center>
          </div>

          <center><h2>Borrow List</h2></center><br><br>

          <table class="table table-striped">
    <thead>
      <tr>
        <th>Id Peminjaman</th>
        <th>Id Peminjam</th>
        <th>Tanggal Peminjaman</th>
        <th>Tanggal Pengembalian</th>
        <th>Nama Barang</th>
        <th>Aksi</th>        
      </tr>
    </thead>
    <tbody>
    <?php 
		foreach($peminjaman as $p){ 
		?>
      <tr>
        <td><?php echo $p->id_peminjaman ?></td>
        <td><?php echo $p->id_peminjam ?></td>
        <td><?php echo $p->tgl_pinjam ?></td>
        <td><?php echo $p->tgl_kembali ?></td>
        <td><?php echo $p->nama_barang ?></td>
        <td>
        <a href="<?= base_url('Pjmadmin/detail/'.$p->id_peminjaman)?>"><img src="assets/image/more.png"></a>
        </td> 
      </tr>
      <?php } ?>

    </tbody>
  </table>
<br><br> 

          
          
          
      </div>
    </div>


    <!-- Optional JavaScript -->      
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script type="text/javascript" src="assets/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="../../assets/bootstrap/js/npm.js"></script>
        <script type="text/javascript" src="../../assets/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../assets/bootstrap/js/jquery.min.js"></script>
        <script type="text/javascript" src="../../assets/bootstrap/js/jsquery.min.js"></script>
        <script type="text/javascript" src="../../assets/bootstrap/js/query.min.js"></script>



        
    </body>
    </html>