<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.min.css"> 
    <link rel="stylesheet" type="text/css" href="assets/css/admin.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <title>Inventory List. Admin</title>

    
  </head>
  <body style="font-family: Montserrat;">

    
      <div class="container-fluid">
      <div class="row">
      <div class="col-sm-3 col-md-2 sidebar" style="background-color: black;">

<ul class="nav nav-sidebar">
  <li><a href="#" style="color:white; font-size: 25pt;margin-top: 60px; margin-left: 20px;">Invent.</a></li>
  <li><a href="#">username</a></li>
  <li><a href="#">Role : Admin</a></li>
  <li><a href="<?= base_url('Landing') ?>">Logout<i class="glyphicon glyphicon-log-out" style="margin-left: 10px;"></i></a></li>
</ul>

<ul class="nav nav-sidebar">
  <li><a href="<?= base_url('Dashadmin') ?>" style="color:white;"><i class="glyphicon glyphicon-home" style="margin-right: 10px;"></i>Home </a></li>
</ul>

<ul class="nav nav-sidebar">
  <li><p href="#" style="color:white; font-size: 18pt; margin-left: 20px; ">Inventory</p></li>
  <li><a href="<?= base_url('Inventadmin') ?>" style="color:white;"><i class="glyphicon glyphicon-list" style="margin-right: 10px;"></i>Inventory List </a></li>
  <li><a href="<?= base_url('Pjmadmin') ?>" style="color:white;"><i class="glyphicon glyphicon-list" style="margin-right: 10px;"></i>Borrow List</a></li>
  <li><a href="<?= base_url('Pkbadmin') ?>" style="color:white;"><i class="glyphicon glyphicon-list" style="margin-right: 10px;"></i>Return List</a></li>
</ul>

<ul class="nav nav-sidebar">
  <li><p href="#" style="color:white; font-size: 18pt; margin-left: 20px; ">User</p></li>
  <li><a href="#" style="color:white;"><i class="glyphicon glyphicon-list" style="margin-right: 10px;"></i>Admin Data </a></li>
  <li><a href="<?= base_url('Ptgadmin') ?>" style="color:white;"><i class="glyphicon glyphicon-list" style="margin-right: 10px;"></i>Officer Data</a></li>
  <li><a href="<?= base_url('Pminjamadmin') ?>" style="color:white;"><i class="glyphicon glyphicon-list" style="margin-right: 10px;"></i>Borrowers Data</a></li>
</ul>


</div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">


          <h1 class="page-header">Dashboard</h1>

          <div class="col-sm-4">
            <center>
              <div class="thumbnail jumlah">
                <h6>number of borrowers today :</h6>
                <h4 style="font-size: 30pt">10</h4>
              </div>
            </center>
          </div>

          <div class="col-sm-4">
            <center>
              <div class="thumbnail jumlah">
                <h6>number of borrows today :</h6>
                <h4 style="font-size: 30pt">10</h4>
              </div>
            </center>
          </div>

          <div class="col-sm-4" style="margin-bottom: 50px;">
            <center>
              <div class="thumbnail jumlah">
                <h6>number of returns today :</h6>
                <h4 style="font-size: 30pt">10</h4>
              </div>
            </center>
          </div>

          <center><h2>Inventory List</h2></center><br><br>

          <table class="table table-striped">
    <thead>
      <tr>
        <th>Id Inventaris</th>
        <th>Id Jenis</th>
        <th>Id Ruang</th>
        <th>Nama</th>
        <th>Jumlah</th>
        <th>Kondisi</th>
        <th>Keterangan</th>
        <th>Kode Inventaris</th>
        <th>Id Petugas</th>
        <th>Tanggal Register</th>
        <th>Status</th>
        <th>Foto</th>
        <th>Aksi</th>        
      </tr>
    </thead>
    <tbody>
    <?php 
		foreach($inventaris as $i){ 
		?>
      <tr>
        <td><?php echo $i->id_inventaris ?></td>
        <td><?php echo $i->id_jenis ?></td>
        <td><?php echo $i->id_ruang ?></td>
        <td><?php echo $i->nama ?></td>
        <td><?php echo $i->jumlah ?></td>
        <td><?php echo $i->kondisi ?></td>
        <td><?php echo $i->keterangan ?></td>
        <td><?php echo $i->kode_inventaris ?></td>
        <td><?php echo $i->id_petugas ?></td>
        <td><?php echo $i->tgl_register ?></td>
        <td><?php echo $i->status ?></td>
        <td><?php echo $i->foto ?></td>
        <td>
          <a href="<?= base_url('Inventadmin/detail/'.$i->id_inventaris)?>"><img src="assets/image/more.png"></a>
          <a href="<?= base_url('Einventaris/edit/'.$i->id_inventaris)?>"><img src="assets/image/ubah.png"></a>
          <a href="<?= base_url('Tinventaris/hapus/'.$i->id_inventaris)?>" onclick="return confirm('Hapus Data?');"><img src="assets/image/hapus.png"></a>
        </td>
      </tr>
      <?php } ?>

    </tbody>
  </table>
<br><br> 


  <a href="<?= base_url('Tinventaris')?>"><center><button type="button" class="btn action"> Insert New Inventory<i class="glyphicon glyphicon-plus-sign" style="margin-left: 10px; color:white;"></i></button></a>
  <a href="<?= base_url('Inventadmin/print')?>"><button type="button" class="btn action" style="margin-left:20px;"> Generate Report<i class="glyphicon glyphicon-print" style="margin-left: 10px; color:white;"></i></button></center></a>
      
          
          
      </div>
    </div>


    <!-- Optional JavaScript -->      
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script type="text/javascript" src="assets/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="../../assets/bootstrap/js/npm.js"></script>
        <script type="text/javascript" src="../../assets/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../assets/bootstrap/js/jquery.min.js"></script>
        <script type="text/javascript" src="../../assets/bootstrap/js/jsquery.min.js"></script>
        <script type="text/javascript" src="../../assets/bootstrap/js/query.min.js"></script>



        
    </body>
    </html>