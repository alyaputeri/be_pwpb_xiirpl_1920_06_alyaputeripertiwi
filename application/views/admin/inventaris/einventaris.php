<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/bootstrap/css/bootstrap.min.css"> 
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/admin.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <title>Insert Inventory. Admin</title>

    
  </head>
  <body style="font-family: Montserrat;">

    
      <div class="container-fluid">
      <div class="row">
      <div class="col-sm-3 col-md-2 sidebar" style="background-color: black;">

<ul class="nav nav-sidebar">
  <li><a href="#" style="color:white; font-size: 25pt;margin-top: 60px; margin-left: 20px;">Invent.</a></li>
  <li><a href="#">username</a></li>
  <li><a href="#">Role : Admin</a></li>
  <li><a href="<?= base_url('Landing') ?>">Logout<i class="glyphicon glyphicon-log-out" style="margin-left: 10px;"></i></a></li>
</ul>

<ul class="nav nav-sidebar">
  <li><a href="<?= base_url('Dashadmin') ?>" style="color:white;"><i class="glyphicon glyphicon-home" style="margin-right: 10px;"></i>Home </a></li>
</ul>

<ul class="nav nav-sidebar">
  <li><p href="#" style="color:white; font-size: 18pt; margin-left: 20px; ">Inventory</p></li>
  <li><a href="<?= base_url('Inventadmin') ?>" style="color:white;"><i class="glyphicon glyphicon-list" style="margin-right: 10px;"></i>Inventory List </a></li>
  <li><a href="<?= base_url('Pjmadmin') ?>" style="color:white;"><i class="glyphicon glyphicon-list" style="margin-right: 10px;"></i>Borrow List</a></li>
  <li><a href="<?= base_url('Pkbadmin') ?>" style="color:white;"><i class="glyphicon glyphicon-list" style="margin-right: 10px;"></i>Return List</a></li>
</ul>

<ul class="nav nav-sidebar">
  <li><p href="#" style="color:white; font-size: 18pt; margin-left: 20px; ">User</p></li>
  <li><a href="#" style="color:white;"><i class="glyphicon glyphicon-list" style="margin-right: 10px;"></i>Admin Data </a></li>
  <li><a href="<?= base_url('Ptgadmin') ?>" style="color:white;"><i class="glyphicon glyphicon-list" style="margin-right: 10px;"></i>Officer Data</a></li>
  <li><a href="<?= base_url('Pminjamadmin') ?>" style="color:white;"><i class="glyphicon glyphicon-list" style="margin-right: 10px;"></i>Borrowers Data</a></li>
</ul>


</div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">


          <h1 class="page-header">Dashboard</h1>
          <div class="col-sm-4">
            <center>
              <div class="thumbnail jumlah">
                <h6>number of borrowers today :</h6>
                <h4 style="font-size: 30pt">10</h4>
              </div>
            </center>
          </div>

          <div class="col-sm-4">
            <center>
              <div class="thumbnail jumlah">
                <h6>number of borrows today :</h6>
                <h4 style="font-size: 30pt">10</h4>
              </div>
            </center>
          </div>

          <div class="col-sm-4" style="margin-bottom: 50px;">
            <center>
              <div class="thumbnail jumlah">
                <h6>number of returns today :</h6>
                <h4 style="font-size: 30pt">10</h4>
              </div>
            </center>
          </div>

          <center><h2> New Inventory</h2></center><br><br>

          <?php foreach($inventaris as $i){ ?>

          <form action="<?= base_url('Einventaris/update') ?>" method="post">

          <div class="col-sm-4">
            
            <div class="form-group">
              <label for="idi">Id Inventaris</label>
              <input type="text" class="form-control" id="idi" name="id_inventaris" readonly value="<?php echo $i->id_inventaris ?>">
            </div>
          
            <div class="form-group">
            <label for="sel1">Id Jenis</label>
            <select class="form-control" id="sel1" name="id_jenis" value="<?php echo $i->id_jenis ?>">
                <?php foreach ($jenis as $j) : ?>
                    <option value="<?= $j['id_jenis'] ?>"><?= $j['nama']; ?></option>
                <?php endforeach; ?>
            </select>
            </div>

          </div>

          <div class="col-sm-4">
            
            <div class="form-group">
                <label for="sel1">Id Ruang</label>
                <select class="form-control" id="sel1" name="id_ruang" value="<?php echo $i->id_ruang ?>">
                    <?php foreach ($ruang as $r) : ?>
                        <option value="<?= $r['id_ruang'] ?>"><?= $r['nama']; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
          
            <div class="form-group">
              <label for="nama">Nama</label>
              <input type="text" class="form-control" id="nama" name="nama" value="<?php echo $i->nama ?>">
            </div>

          </div>

          <div class="col-sm-4">
            
            <div class="form-group">
              <label for="jumlah">Jumlah</label>
              <input type="text" class="form-control" id="jumlah" name="jumlah" value="<?php echo $i->jumlah ?>">
            </div>
          
            <div class="form-group">
                <label for="sel1">Kondisi</label>
                <select class="form-control" id="sel1" name="kondisi"  value="<?php echo $i->kondisi ?>">
                    <option value="Baik">Baik</option>
                    <option value="Rusak">Rusak</option>
                </select>
            </div>
        </div>

        <div class="col-sm-4">
            
            <div class="form-group">
              <label for="keterangan">Keterangan</label>
              <input type="text" class="form-control" id="keterangan" name="keterangan" value="<?php echo $i->keterangan ?>">
            </div>
          
            <div class="form-group">
              <label for="kode_inventaris">Kode Inventaris</label>
              <input type="text" class="form-control" id="kode_inventaris" name="kode_inventaris" value="<?php echo $i->kode_inventaris ?>">
            </div>

        </div>

        <div class="col-sm-4">
            
            <div class="form-group">
                <label for="sel1">Id Petugas</label>
                <select class="form-control" id="sel1" name="id_petugas" value="<?php echo $i->id_petugas ?>">
                    <?php foreach ($petugas as $p) : ?>
                        <option value="<?= $p['id_petugas'] ?>"><?= $p['nama_petugas']; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
          
            <div class="form-group">
              <label for="tgl_register">Tanggal Register</label>
              <input type="date" class="form-control" id="tgl_register" name="tgl_register" value="<?php echo $i->tgl_register ?>">
            </div>

        </div>

        <div class="col-sm-4">
            
        <div class="form-group">
                <label for="sel1">Status</label>
                <select class="form-control" id="sel1" name="status" value="<?php echo $i->status ?>">
                    <option value="Tersedia">Tersedia</option>
                    <option value="Tidak Tersedia">Tidak Tersedia</option>
                </select>
            </div>

            <div class="form-group">
              <label for="foto">foto</label>
              <input type="file" class="form-control" id="foto" name="foto" value="<?php echo $i->foto ?>">
            </div>

        </div>
       

        
          
<br><br> 
  <center><input type="submit" value="Update" onclick="return confirm('Simpan Perubahan?');">
  </form> 

  <?php } ?> 
          
          
      </div>
    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script type="text/javascript" src="../../assets/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="../../assets/bootstrap/js/npm.js"></script>
    <script type="text/javascript" src="../../assets/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../assets/bootstrap/js/jquery.min.js"></script>
    <script type="text/javascript" src="../../assets/bootstrap/js/jsquery.min.js"></script>
    <script type="text/javascript" src="../../assets/bootstrap/js/query.min.js"></script>



    
  </body>
</html>