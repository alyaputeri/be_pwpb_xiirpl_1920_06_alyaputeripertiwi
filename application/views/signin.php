<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.min.css"> 
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <title>Invent. Sign In</title>

    
  </head>
  <body id="bg" style="background-image: url('assets/image/bg1.jpg');">

    <center>
        <img src="assets/image/s2.png" class="logo">  
        <p class="since">Since 2019 at SMK Negeri 1 Bekasi</p>
        <br>
        <br>

        

          <form action="<?= base_url('Auth/proses_login') ?>" method="post" class="formin">
            <div class="form-group">
              <input type="text" class="form-control ff" id="username" placeholder="username" name="username">
            </div>

            <div class="form-group">
              <input type="password" class="form-control ff" id="pwd" placeholder="password" name="password">
              <p class="forgot">forgot your password?</p>
            </div>

          <button type="submit" class="btn si">Sign In</button>
          <?php  if(isset($pesan)){
                echo $pesan;
              }?>
          </form>

            <br>
          <p class="dont">Dont Have Account? <a href="signup.html"><u>Sign Up Now</u></a></p>
      

        
    </center>
    
        



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script type="text/javascript" src="assets/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="assets/bootstrap/js/npm.js"></script>
    <script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
    
  </body>
</html>