<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pkbadmin extends CI_Controller {

	
	function __construct(){
		parent::__construct();		
		$this->load->model('pjm_data');
                $this->load->helper('url');
	}
 
	function index(){
		$data['pengembalian'] = $this->pjm_data->tampil_datakb  ()->result();
		$this->load->view('admin/pengembalian/list',$data);
    }



	function detail($id_pengembalian){
        $where = array('id_pengembalian' => $id_pengembalian);
        $data['pengembalian'] = $this->pjm_data->detail_data($where,'pengembalian')->result();
        $this->load->view('admin/pengembalian/dpkb',$data);
    }




}
