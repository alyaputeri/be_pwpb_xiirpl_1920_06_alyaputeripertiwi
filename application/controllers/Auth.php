<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('session');

    }

	
	public function index()
	{
		$this->load->view('signin');
	}

	function proses_login(){


        $user=$this->input->post('username');
        $pass=$this->input->post('password');
        $where=array(
            'username'=> $user,
            'password'=> $pass,
        );


        $userpjm= $this->db->get_where('peminjam', ['username'=>$user])->row_array();
        if($userpjm){
            //cek pass
            if ($userpjm['password']== $pass){
                $data= [
                    'username'=> $user ['username'],
                    'id_level'=> $user ['id_level']
                ];
                $this->session->set_userdata($data);
                redirect('Home');
            }
        }else{
            $userptg= $this->db->get_where('petugas', ['username'=>$user])->row_array();
            if($userptg){
                //cek pass
                if ($userptg['password']== $pass && $userptg['id_level']== 2){
                    $data= [
                        'username'=> $userptg ['username'],
                        'id_level'=> $userptg ['id_level']
                    ];
                    $this->session->set_userdata($data);
                    redirect('Dashpetugas');
        } else{
            $useradm= $this->db->get_where('petugas', ['username'=>$user])->row_array();
            if($useradm){
                //cek pass
                if ($useradm['password']== $pass && $useradm['id_level']== 1){
                    $data= [
                        'username'=> $userpetugas ['username'],
                        'id_level'=> $userpetugas ['id_level']
                    ];  
                    $this->session->set_userdata($data);
                    redirect('Dashadmin'); 
                    }else{
                        $data['pesan']="Username atau Password tidak sesuai.";
                        $this->load->view('Auth',$data);
                        }
            }
        }
    }
}
}
}
