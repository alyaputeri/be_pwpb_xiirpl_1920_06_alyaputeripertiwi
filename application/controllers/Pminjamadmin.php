<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pminjamadmin extends CI_Controller {

	
	function __construct(){
		parent::__construct();		
		$this->load->model('user_data');
                $this->load->helper('url');
	}
 
	function index(){
		$data['peminjam'] = $this->user_data->tampil_datapjm ()->result();
		$this->load->view('admin/user/peminjam/list',$data);
    }



	function detail($id_peminjam){
        $where = array('id_peminjam' => $id_peminjam);
        $data['peminjam'] = $this->user_data->detail_data($where,'peminjam')->result();
        $this->load->view('admin/user/peminjam/dpjm',$data);
    }

    function tambah(){
        $data['level'] = $this->db->get('level')->result_array();
		$this->load->view('admin/user/peminjam/tpjm', $data);
    }
    
    function tambah_proses(){
        $id_peminjam = $this->input->post('id_peminjam');
        $nama = $this->input->post('nama');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $status = $this->input->post('status');
        $id_level = $this->input->post('id_level');


		$data = array(
            'id_peminjam' => $id_peminjam,
            'nama' => $nama,
            'username' => $username,
            'password' => $password,
            'status' => $status,
            'id_level' => $id_level

            
			);
		$this->user_data->input_data($data,'peminjam');
        redirect('Pminjamadmin');
        
    }   

    function edit($id_peminjam){
        $data['level'] = $this->db->get('level')->result_array();
        $where = array('id_peminjam' => $id_peminjam);
        $data['peminjam'] = $this->user_data->edit_data($where,'peminjam')->result();
        $this->load->view('admin/user/peminjam/epjm',$data);
    }

    function update(){
        $id_peminjam = $this->input->post('id_peminjam');
        $nama = $this->input->post('nama');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $status = $this->input->post('status');
        $id_level = $this->input->post('id_level');


		$data = array(
            'id_peminjam' => $id_peminjam,
            'nama' => $nama,
            'username' => $username,
            'password' => $password,
            'status' => $status,
            'id_level' => $id_level

            
			);
    
        $where = array(
            'id_peminjam' => $id_peminjam
        );
    
        $this->user_data->update_data($where,$data,'peminjam');
        redirect('Pminjamadmin');
    }

    function hapus($id_peminjam){
		$where = array('id_peminjam' => $id_peminjam);
		$this->user_data->hapus_data($where,'peminjam');
		redirect('Pminjamadmin');
    }




}
