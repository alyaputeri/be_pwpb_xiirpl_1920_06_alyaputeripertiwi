<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pkbpetugas extends CI_Controller {

	
	function __construct(){
		parent::__construct();		
		$this->load->model('pkb_ptg');
                $this->load->helper('url');
	}
 
	function index(){
		$data['pengembalian'] = $this->pkb_ptg->tampil_data()->result();
		$this->load->view('petugas/pengembalian/list',$data);
    }

     function tambah(){
         $data['v_peminjaman'] = $this->db->get('v_peminjaman')->result_array();
         $data['peminjaman'] = $this->db->get('peminjaman')->result_array();
         $data['peminjam'] = $this->db->get('peminjam')->result_array();
         $data['petugas'] = $this->db->get('petugas')->result_array();
         $data['inventaris'] = $this->db->get('inventaris')->result_array();
	 	$this->load->view('petugas/pengembalian/tpkb', $data);
     }
    
     function tambah_proses(){
         $id_pengembalian = $this->input->post('$id_pengembalian');
         $id_inventaris = $this->input->post('id_inventaris');
         $id_petugas = $this->input->post('id_petugas');
         $id_peminjam = $this->input->post('id_peminjam');
         $nama_barang = $this->input->post('nama_barang');
         $jumlah = $this->input->post('jumlah');
         $id_peminjaman = $this->input->post('id_peminjaman');
 
	 	$data = array(
             'id_pengembalian' => $id_pengembalian,
             'id_inventaris' => $id_inventaris,
             'id_petugas' => $id_petugas,
             'id_peminjam' => $id_peminjam,
             'nama_barang' => $nama_barang,
             'jumlah' => $jumlah,
             'id_peminjaman' => $id_peminjaman

	 		);
	 	$this->pkb_ptg->input_data($data,'pengembalian');
	 	redirect('Pkbpetugas');
     }  
     
     function tambah_dpjm(){
        $data['peminjaman'] = $this->db->get('peminjaman')->result_array();
        $data['inventaris'] = $this->db->get('inventaris')->result_array();
        $data['petugas'] = $this->db->get('petugas')->result_array();
        $this->load->view('petugas/peminjaman/tdpjm', $data);
    }
   
    function tambah_dpjm_proses(){
        $id_detailpinjam = $this->input->post('id_detailpinjam');
        $id_peminjaman = $this->input->post('id_peminjaman');
        $id_inventaris = $this->input->post('id_inventaris');
        $id_petugas = $this->input->post('id_petugas');
        $jumlah = $this->input->post('jumlah');

        $data = array(
            'id_detailpinjam' => $id_detailpinjam,
            'id_peminjaman' => $id_peminjaman,
            'id_inventaris' => $id_inventaris,
            'id_petugas' => $id_petugas,
            'jumlah' => $jumlah
            );
        $this->pkb_ptg->input_data($data,'detail_pinjam');
        redirect('Pjmpetugas');
    }  

	function detail($id_pengembalian){
        $where = array('id_pengembalian' => $id_pengembalian);
        $data['pengembalian'] = $this->pkb_ptg->detail_data($where,'pengembalian')->result();
        $this->load->view('petugas/pengembalian/dpkb',$data);
    }




}
