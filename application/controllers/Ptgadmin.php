<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ptgadmin extends CI_Controller {

	
	function __construct(){
		parent::__construct();		
		$this->load->model('user_data');
                $this->load->helper('url');
	}
 
	function index(){
		$data['petugas'] = $this->user_data->tampil_data()->result();
		$this->load->view('admin/user/petugas/list',$data);
    }



	function detail($id_petugas){
        $where = array('id_petugas' => $id_petugas);
        $data['petugas'] = $this->user_data->detail_data($where,'petugas')->result();
        $this->load->view('admin/user/petugas/dptg',$data);
    }

    function tambah(){
        $data['level'] = $this->db->get('level')->result_array();
		$this->load->view('admin/user/petugas/tptg', $data);
    }
    
    function tambah_proses(){
        $id_petugas = $this->input->post('id_petugas');
        $id_level = $this->input->post('id_level');
        $nama_petugas = $this->input->post('nama_petugas');
        $username = $this->input->post('username');
        $password = $this->input->post('password');


		$data = array(
            'id_petugas' => $id_petugas,
            'id_level' => $id_level,
            'nama_petugas' => $nama_petugas,
            'username' => $username,
            'password' => $password
            
			);
		$this->user_data->input_data($data,'petugas');
        redirect('Ptgadmin');
        
    }   

    function edit($id_petugas){
        $data['level'] = $this->db->get('level')->result_array();
        $where = array('id_petugas' => $id_petugas);
        $data['petugas'] = $this->user_data->edit_data($where,'petugas')->result();
        $this->load->view('admin/user/petugas/eptg',$data);
    }

    function update(){
        $id_petugas = $this->input->post('id_petugas');
        $id_level = $this->input->post('id_level');
        $nama_petugas = $this->input->post('nama_petugas');
        $username = $this->input->post('username');
        $password = $this->input->post('password');


		$data = array(
            'id_petugas' => $id_petugas,
            'id_level' => $id_level,
            'nama_petugas' => $nama_petugas,
            'username' => $username,
            'password' => $password
            
			);
    
        $where = array(
            'id_petugas' => $id_petugas
        );
    
        $this->user_data->update_data($where,$data,'petugas');
        redirect('Ptgadmin');
    }

    function hapus($id_petugas){
		$where = array('id_petugas' => $id_petugas);
		$this->user_data->hapus_data($where,'petugas');
		redirect('Ptgadmin');
    }




}
