<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashadmin extends CI_Controller {

	function __construct(){

		parent::__construct();		
		$this->load->model('jml_data');
                $this->load->helper('url');
	}

	public function index()
	{	
		$data['jml_peminjaman'] = $this->jml_data->tampil_data()->result();
		$this->load->view('admin/index');
	}


}
