<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tinventaris extends CI_Controller {

	
	function __construct(){
		parent::__construct();		
		$this->load->model('m_data');
                $this->load->helper('url');
	}
 
	function index(){
        $data['jenis'] = $this->db->get('jenis')->result_array();
        $data['ruang'] = $this->db->get('ruang')->result_array();
        $data['petugas'] = $this->db->get('petugas')->result_array();
		$this->load->view('admin/inventaris/tinventaris', $data);
    }
    
    function tambah(){
        $id_inventaris = $this->input->post('id_inventaris');
        $id_jenis = $this->input->post('id_jenis');
        $id_ruang = $this->input->post('id_ruang');
        $nama = $this->input->post('nama');
        $jumlah = $this->input->post('jumlah');
        $kondisi = $this->input->post('kondisi');
        $keterangan = $this->input->post('keterangan');
        $kode_inventaris = $this->input->post('kode_inventaris');
        $id_petugas = $this->input->post('id_petugas');
        $tgl_register = $this->input->post('tgl_register');
        $status = $this->input->post('status');
        $foto = $this->input->post('foto');
 
		$data = array(
            'id_inventaris' => $id_inventaris,
            'id_jenis' => $id_jenis,
            'id_ruang' => $id_ruang,
            'nama' => $nama,
            'jumlah' => $jumlah,
            'kondisi' => $kondisi,
            'keterangan' => $keterangan,
            'kode_inventaris' => $kode_inventaris,
            'id_petugas' => $id_petugas,
            'tgl_register' => $tgl_register,
            'status' => $status,
            'foto' => $foto
			);
		$this->m_data->input_data($data,'inventaris');
		redirect('Inventadmin');
    }   
    
    function hapus($id_inventaris){
		$where = array('id_inventaris' => $id_inventaris);
		$this->m_data->hapus_data($where,'inventaris');
		redirect('Inventadmin');
    }
    
    


}
