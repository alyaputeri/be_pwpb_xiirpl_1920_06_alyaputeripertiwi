<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventadmin extends CI_Controller {

	
	function __construct(){
		parent::__construct();		
		$this->load->model('m_data');
                $this->load->helper('url');
	}
 
	function index(){
		$data['jenis'] = $this->db->get('jenis')->result_array();
		$data['inventaris'] = $this->m_data->tampil_data()->result();
		$this->load->view('admin/inventaris/inventaris',$data);
	}

	function detail($id_inventaris){
		$data['jenis'] = $this->db->get('jenis')->result_array();
        $where = array('id_inventaris' => $id_inventaris);
        $data['inventaris'] = $this->m_data->edit_data($where,'inventaris')->result();
        $this->load->view('admin/inventaris/dinventaris',$data);
	}
	
	public function print(){
		$data['inventaris'] = $this->m_data->tampil_data('inventaris')->result();
		$this->load->view('admin/inventaris/report_inventaris',$data);
	}


}
