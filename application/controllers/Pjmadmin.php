<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pjmadmin extends CI_Controller {

	
	function __construct(){
		parent::__construct();		
		$this->load->model('pjm_data');
                $this->load->helper('url');
	}
 
	function index(){
		$data['peminjaman'] = $this->pjm_data->tampil_data()->result();
		$this->load->view('admin/peminjaman/list',$data);
    }

    // function tambah(){
    //     $data['peminjam'] = $this->db->get('peminjam')->result_array();
    //     $data['inventaris'] = $this->db->get('inventaris')->result_array();
	// 	$this->load->view('admin/peminjaman/tpjm', $data);
    // }
    
    // function tambah_proses(){
    //     $id_peminjaman = $this->input->post('id_peminjaman');
    //     $id_peminjam = $this->input->post('id_peminjam');
    //     $tgl_pinjam = $this->input->post('tgl_pinjam');
    //     $tgl_kembali = $this->input->post('tgl_kembali');
    //     $nama_barang = $this->input->post('nama_barang');
 
	// 	$data = array(
    //         'id_peminjaman' => $id_peminjaman,
    //         'id_peminjam' => $id_peminjam,
    //         'tgl_pinjam' => $tgl_pinjam,
    //         'tgl_kembali' => $tgl_kembali,
	// 		'nama_barang' => $nama_barang
	// 		);
	// 	$this->pjm_data->input_data($data,'peminjaman');
	// 	redirect('Pjmadmin');
    // }   

	function detail($id_peminjaman){
        $where = array('id_peminjaman' => $id_peminjaman);
        $data['peminjaman'] = $this->pjm_data->detail_data($where,'peminjaman')->result();
        $this->load->view('admin/peminjaman/dpjm',$data);
    }




}
