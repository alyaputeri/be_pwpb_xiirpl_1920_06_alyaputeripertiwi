<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pjmpetugas extends CI_Controller {

	
	function __construct(){
		parent::__construct();		
		$this->load->model('pjm_ptg');
                $this->load->helper('url');
	}
 
	function index(){
		$data['v_peminjaman'] = $this->pjm_ptg->tampil_data()->result();
		$this->load->view('petugas/peminjaman/list',$data);
    }

     function tambah_pjm(){
         $data['peminjam'] = $this->db->get('peminjam')->result_array();
         $data['inventaris'] = $this->db->get('inventaris')->result_array();
	 	$this->load->view('petugas/peminjaman/tpjm', $data);
     }
    
     function tambah_pjm_proses(){
         $id_peminjaman = $this->input->post('id_peminjaman');
         $id_peminjam = $this->input->post('id_peminjam');
         $tgl_pinjam = $this->input->post('tgl_pinjam');
         $tgl_kembali = $this->input->post('tgl_kembali');
         $nama_barang = $this->input->post('nama_barang');
 
	 	$data = array(
             'id_peminjaman' => $id_peminjaman,
             'id_peminjam' => $id_peminjam,
             'tgl_pinjam' => $tgl_pinjam,
             'tgl_kembali' => $tgl_kembali,
	 		'nama_barang' => $nama_barang
	 		);
	 	$this->pjm_ptg->input_data($data,'peminjaman');
	 	redirect('Pjmpetugas/tambah_dpjm');
     }  
     
     function tambah_dpjm(){
        $data['peminjaman'] = $this->db->get('peminjaman')->result_array();
        $data['inventaris'] = $this->db->get('inventaris')->result_array();
        $data['petugas'] = $this->db->get('petugas')->result_array();
        $this->load->view('petugas/peminjaman/tdpjm', $data);
    }
   
    function tambah_dpjm_proses(){
        $id_detailpinjam = $this->input->post('id_detailpinjam');
        $id_peminjaman = $this->input->post('id_peminjaman');
        $id_inventaris = $this->input->post('id_inventaris');
        $id_petugas = $this->input->post('id_petugas');
        $jumlah = $this->input->post('jumlah');

        $data = array(
            'id_detailpinjam' => $id_detailpinjam,
            'id_peminjaman' => $id_peminjaman,
            'id_inventaris' => $id_inventaris,
            'id_petugas' => $id_petugas,
            'jumlah' => $jumlah
            );
        $this->pjm_ptg->input_data($data,'detail_pinjam');
        redirect('Pjmpetugas');
    }  

	function detail($id_peminjaman){
        $where = array('id_peminjaman' => $id_peminjaman);
        $data['v_peminjaman'] = $this->pjm_ptg->detail_data($where,'v_peminjaman')->result();
        $this->load->view('petugas/peminjaman/dpjm',$data);
    }




}
